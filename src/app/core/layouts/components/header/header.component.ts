import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AuthentificationService} from "../../../services/authentification.service";
import {UserI} from "../../../interfaces/user-i";
import {UsersService} from "../../../../features/user-connected/users/users.service";
import {Router} from "@angular/router";
import {User} from "../../../models/user";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public connectedUser?: User | null;
  public menuOpenStatus = true;

  @Output() menuToggle = new EventEmitter<boolean>();

  constructor(private authentificationService: AuthentificationService,
              private userService: UsersService,
              private router: Router) { }

  ngOnInit(): void {
    this.authentificationService.onConnectionChange$().subscribe( {
      next: (userStatus) => {
        this.connectedUser = userStatus;
      }
    })
  }

  disconnect() {
    //this.authentificationService.disconnect();
    this.userService.logout().subscribe( () => {
        this.router.navigate(['/'])
    }

    );
  }

  toggleMenu() {
    this.menuOpenStatus = !this.menuOpenStatus;
    this.menuToggle.emit(this.menuOpenStatus);
  }

}
