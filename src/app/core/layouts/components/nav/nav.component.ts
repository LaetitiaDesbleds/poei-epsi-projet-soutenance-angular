import { Component, OnInit } from '@angular/core';
import {AuthentificationService} from "../../../services/authentification.service";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  public connected = false;

  constructor(private authentificationService: AuthentificationService) { }

  ngOnInit(): void {
    this.authentificationService.onConnectionChange$().subscribe(
      (user) => this.connected = !!user
    )
  }

}
