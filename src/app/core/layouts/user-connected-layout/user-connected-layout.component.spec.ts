import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserConnectedLayoutComponent } from './user-connected-layout.component';

describe('UserConnectedLayoutComponent', () => {
  let component: UserConnectedLayoutComponent;
  let fixture: ComponentFixture<UserConnectedLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserConnectedLayoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserConnectedLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
