import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-connected-layout',
  templateUrl: './user-connected-layout.component.html',
  styleUrls: ['./user-connected-layout.component.scss']
})
export class UserConnectedLayoutComponent implements OnInit {

  openMenu = true;

  constructor() { }

  ngOnInit(): void {
  }

  toggleMenu() {
    this.openMenu = !this.openMenu;
  }

}
