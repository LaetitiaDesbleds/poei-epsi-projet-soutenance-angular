import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IconsModule} from "../icons/icons.module";
import {TemplatesModule} from "../templates/templates.module";
import {FooterComponent} from './layouts/components/footer/footer.component';
import {HeaderComponent} from './layouts/components/header/header.component';
import {NavComponent} from './layouts/components/nav/nav.component';
import {RouterModule} from "@angular/router";
import {UserConnectedLayoutComponent} from './layouts/user-connected-layout/user-connected-layout.component';
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {AuthorizationInterceptor} from "./interceptors/authorization.interceptor";

@NgModule({
  declarations: [
    FooterComponent,
    HeaderComponent,
    NavComponent,
    UserConnectedLayoutComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    IconsModule
  ],
  exports: [
    IconsModule,
    TemplatesModule,
    FooterComponent,
    HeaderComponent,
    NavComponent,
    UserConnectedLayoutComponent,
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthorizationInterceptor, multi: true}
  ]
})
export class CoreModule {
}
