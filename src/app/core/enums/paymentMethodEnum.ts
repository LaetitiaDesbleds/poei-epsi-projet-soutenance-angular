export enum PaymentMethodEnum{
  CB='CB',
  CASH="Liquide",
  CHECK="Chèque",
  DEFERRED_PAYMENT="Paiement différé"
}
