export enum JobUser {
  DIRECTEUR = 'Directeur',
  COMPTABLE = 'Comptable',
  COMMERCIAL = 'Commercial'
}
