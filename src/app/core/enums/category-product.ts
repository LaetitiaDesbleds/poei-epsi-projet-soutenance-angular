export enum CategoryProduct {
  VAN = 'Van',
  CAPUCINE = 'Capucine',
  INTEGRAL = 'Intégral',
  FOURGON = 'Fourgon'
}
