export enum OrderStateEnum {

  FINISHED = 'Terminé',
  ORDERED = 'En cours'
}
