import {Injectable} from '@angular/core';
import {BehaviorSubject, map, Observable, ReplaySubject, tap} from "rxjs";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {User} from "../models/user";
import {environment} from "../../../environments/environment";
import {UserI} from "../interfaces/user-i";
import {Router} from "@angular/router";
import {RoleUser} from "../enums/role-user";

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

  private url = environment.apiUrl;

  private user?: User;

  public connectedUser$: BehaviorSubject<User | null>;

  get userCredential() {
    return this.user ? btoa(this.user.username + ':' + this.user.password) : null;
  }

  constructor(private http: HttpClient, private router: Router) {
    const userInStorage = localStorage.getItem('user');
    this.connectedUser$ = new BehaviorSubject<User | null>(userInStorage ? JSON.parse(userInStorage) : null);
  }

  private emitUser() {
    this.connectedUser$.next(this.user || null);
  }

  public onConnectionChange$() {
    return this.connectedUser$.asObservable();
  }

  public disconnect() {
    this.http.get<User>(`${this.url}/logout`).subscribe((value) => {
      this.user = undefined;
      this.emitUser();
      this.router.navigate(['/', 'login']);
    });
  }

  public login(username: string, password: string): Observable<User> {
    const headers = new HttpHeaders({
      authorization: 'Basic ' + btoa(username+':'+password)
    })

    const params = new HttpParams()
      .set('username', username)
      .set('password', password);

    return this.http.get<User>(`${this.url}/users/login`, {headers, params}).pipe(
      tap((value) => {
        localStorage.setItem('user', JSON.stringify(value));
        this.user = value;
        this.emitUser();
      })
    );
  }

  public isAdmin(): Observable<boolean> {
    return this.connectedUser$
      .pipe(
        map(user => user?.grants === RoleUser.ADMIN)
      )
  }
}
