import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {AuthentificationService} from "../services/authentification.service";

@Injectable()
export class AuthorizationInterceptor implements HttpInterceptor {

  constructor(private authentification: AuthentificationService) {
  }

  intercept(httpRequest: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (!httpRequest.withCredentials) {
      return next.handle(httpRequest);
    }

    if (!this.authentification.userCredential) {
      return next.handle(httpRequest);
    }

    return next.handle(this.addAuthorizationHeader(httpRequest));
  }

  private addAuthorizationHeader(request: HttpRequest<any>) {
    return request.clone({
      setHeaders: {
        Authorization: 'Basic ' + this.authentification.userCredential
      }
    });
  }
}
