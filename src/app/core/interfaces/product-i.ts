import {GearboxProduct} from "../enums/gearbox-product";
import {CategoryProduct} from "../enums/category-product";
import {PictureProduct} from "../enums/picture-product";

export interface ProductI {
  id: number;
  denomination: string;
  price_ttc: number;
  gear_box: GearboxProduct;
  category: CategoryProduct;
  numberOfBeds: number;
  picture: PictureProduct;
  yearOfManufacture: Date;
}
