import {RoleUser} from "../enums/role-user";
import {JobUser} from "../enums/job-user";

export interface UserI {
  id: number;
  employeeNumber: number;
  username: string;
  lastname: string;
  firstname: string;
  email: string;
  password: string;
  grants: RoleUser;
  job: JobUser;
}
