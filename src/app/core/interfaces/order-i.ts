import {ProductOrderI} from "./productOrder-i";
import {CustomerOrderI} from "./customer-order-i";
import {PaymentMethodEnum} from "../enums/paymentMethodEnum";
import {OrderStateEnum} from "../enums/orderStateEnum";

export interface OrderI {
  id: number;
  orderDate: string;
  orderState: OrderStateEnum;
  paymentMethod: PaymentMethodEnum;
  customerId?: CustomerOrderI;
  productId?: ProductOrderI;
  customerDto?: CustomerOrderI;
  productDto?: ProductOrderI;
  orderNotes: string;
}


