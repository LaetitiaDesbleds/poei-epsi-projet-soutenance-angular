export interface CustomerI {
  id: number;
  lastname: string;
  firstname: string;
  email: string;
  address?: string;
  mobile: string;
  status: boolean;
  notes?: string;
}
