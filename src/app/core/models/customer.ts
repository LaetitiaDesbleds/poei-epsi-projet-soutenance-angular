import {CustomerI} from "../interfaces/customer-i";



export class Customer implements CustomerI {
  id = 0;
  lastname = '';
  firstname = '';
  email = '';
  address = '';
  mobile= '';
  status = false;
  notes = '';

  /**
   *
   * @param obj objet partiel
   */
  constructor(obj? : Partial<Customer>) {
    if (obj) {
      Object.assign(this, obj);
    }
  }
}
