import {ProductOrder} from "./productOrder";
import {OrderI} from "../interfaces/order-i";
import {CustomerOrder} from "./customerOrder";
import {OrderStateEnum} from "../enums/orderStateEnum";
import {PaymentMethodEnum} from "../enums/paymentMethodEnum";

export class OrderDisplay implements OrderI {

  id = 0;
  orderDate = '';
  orderState = OrderStateEnum.ORDERED;
  paymentMethod = PaymentMethodEnum.CB;
  customerDto = new CustomerOrder()
  productDto = new ProductOrder();
  orderNotes!: string;

  /**
   *
   * @param obj objet partiel
   */
  constructor(obj?: Partial<OrderDisplay>) {
    if (obj) {
      Object.assign(this, obj);
    }
  }


}


