import {OrderI} from "../interfaces/order-i";
import {Customer} from "./customer";
import {Product} from "./product";
import {OrderStateEnum} from "../enums/orderStateEnum";
import {PaymentMethodEnum} from "../enums/paymentMethodEnum";

export class Order implements OrderI {

  id = 0;
  orderDate = '';
  orderState = OrderStateEnum.ORDERED;
  paymentMethod = PaymentMethodEnum.CB;
  customerId = new Customer()
  productId = new Product();
  orderNotes!: string;

  /**
   *
   * @param obj objet partiel
   */
  constructor(obj?: Partial<Order>) {
    if (obj) {
      Object.assign(this, obj);
    }
  }


}


