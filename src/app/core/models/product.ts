import { ProductI } from "../interfaces/product-i";
import {GearboxProduct} from "../enums/gearbox-product";
import {CategoryProduct} from "../enums/category-product";
import {PictureProduct} from "../enums/picture-product";

export class Product implements ProductI {
  id = 0;
  denomination = '';
  price_ttc = 0.0;
  gear_box = GearboxProduct.AUTOMATIQUE;
  category = CategoryProduct.VAN;
  numberOfBeds = 0;
  picture = PictureProduct.PICTURE1;
  yearOfManufacture = new Date();

  /**
   *
   * @param obj objet partiel
   */
  constructor(obj?: Partial<Product>) {
    if (obj) {
      Object.assign(this, obj);
    }
  }

}
