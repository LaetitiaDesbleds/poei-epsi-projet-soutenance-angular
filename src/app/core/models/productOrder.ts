import {ProductOrderI} from "../interfaces/productOrder-i";

export class ProductOrder implements ProductOrderI {
  denomination: string = '';
  price_ttc: number = 0;

  constructor(obj?: Partial<ProductOrderI>) {
    if (obj) {
      Object.assign(this, obj);
    }

  }

}
