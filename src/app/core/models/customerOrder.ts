import {CustomerOrderI} from "../interfaces/customer-order-i";

export class CustomerOrder implements CustomerOrderI {
  firstname: string = '';
  lastname: string = '';

  /**
   *
   * @param obj objet partiel
   */
  constructor(obj?: Partial<CustomerOrderI>) {
    if (obj) {
      Object.assign(this, obj);
    }
  }
}
