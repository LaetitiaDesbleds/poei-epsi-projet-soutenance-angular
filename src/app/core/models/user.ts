import {UserI} from "../interfaces/user-i";
import {RoleUser} from "../enums/role-user";
import {JobUser} from "../enums/job-user";

export class User implements UserI {
  id = 0;
  employeeNumber = 0;
  username = '';
  lastname = '';
  firstname = '';
  email = '';
  password = '' ;
  grants = RoleUser.USER ;
  job = JobUser.COMMERCIAL;


  /**
   *
   * @param obj objet partiel
   */
  constructor(obj?: Partial<User>) {
    if (obj) {
      Object.assign(this, obj);
    }
  }

}
