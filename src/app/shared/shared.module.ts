import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableLightComponent } from './components/table-light/table-light.component';
import { ConfirmationModalComponent } from './components/confirmation-modal/confirmation-modal.component';
import {IconsModule} from "../icons/icons.module";



@NgModule({
    declarations: [
        TableLightComponent,
        ConfirmationModalComponent
    ],
    exports: [
        TableLightComponent,
        ConfirmationModalComponent,
        IconsModule
    ],
    imports: [
        CommonModule
    ]
})
export class SharedModule { }
