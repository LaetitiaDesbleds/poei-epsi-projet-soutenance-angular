import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {
    path: 'login', loadChildren:
      () => import('./features/login/login.module')
        .then((module_) => module_.LoginModule)
  },
  {
    path: 'app',
    loadChildren:
      () => import('./features/user-connected/user-connected.module')
        .then((module_) => module_.UserConnectedModule)
  },
  {path: '', pathMatch: 'full', redirectTo: 'login'},
  {
    path: '**', loadChildren:
      () => import('./features/page-not-found/page-not-found.module')
        .then((module_) => module_.PageNotFoundModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    {preloadingStrategy: PreloadAllModules, onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
