import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconCloseComponent } from './components/icon-close/icon-close.component';
import { IconDeleteComponent } from './components/icon-delete/icon-delete.component';
import { IconEditComponent } from './components/icon-edit/icon-edit.component';
import { IconNavComponent } from './components/icon-nav/icon-nav.component';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import { IconHomeComponent } from './components/icon-home/icon-home.component';
import { IconClientComponent } from './components/icon-client/icon-client.component';
import { IconChartComponent } from './components/icon-chart/icon-chart.component';
import { IconOrderComponent } from './components/icon-order/icon-order.component';
import { IconProductComponent } from './components/icon-product/icon-product.component';
import { IconUserComponent } from './components/icon-user/icon-user.component';
import { IconSearchComponent } from './components/icon-search/icon-search.component';
import { IconDisconnectComponent } from './components/icon-disconnect/icon-disconnect.component';
import { IconAddComponent } from './components/icon-add/icon-add.component';



@NgModule({
  declarations: [
    IconCloseComponent,
    IconDeleteComponent,
    IconEditComponent,
    IconNavComponent,
    IconHomeComponent,
    IconClientComponent,
    IconChartComponent,
    IconOrderComponent,
    IconProductComponent,
    IconUserComponent,
    IconSearchComponent,
    IconDisconnectComponent,
    IconAddComponent
  ],
  imports: [
    CommonModule,
    FontAwesomeModule
  ],
  exports: [
    IconCloseComponent,
    IconDeleteComponent,
    IconEditComponent,
    IconNavComponent,
    IconHomeComponent,
    IconClientComponent,
    IconChartComponent,
    IconOrderComponent,
    IconProductComponent,
    IconUserComponent,
    IconSearchComponent,
    IconDisconnectComponent,
    IconAddComponent
  ]
})
export class IconsModule { }
