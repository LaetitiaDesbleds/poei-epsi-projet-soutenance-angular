import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IconClientComponent } from './icon-client.component';

describe('IconClientComponent', () => {
  let component: IconClientComponent;
  let fixture: ComponentFixture<IconClientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IconClientComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IconClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
