import { Component, OnInit } from '@angular/core';
import {faAddressBook, faBars, faBookBookmark, faUser, IconDefinition} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-icon-client',
  templateUrl: './icon-client.component.html',
  styleUrls: ['./icon-client.component.scss']
})
export class IconClientComponent implements OnInit {

  public myIcon: IconDefinition = faAddressBook;
  constructor() { }

  ngOnInit(): void {
  }

}
