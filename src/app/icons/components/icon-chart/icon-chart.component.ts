import { Component, OnInit } from '@angular/core';
import {faEdit, IconDefinition} from "@fortawesome/free-solid-svg-icons";
import {faChartPie} from "@fortawesome/free-solid-svg-icons/faChartPie";

@Component({
  selector: 'app-icon-chart',
  templateUrl: './icon-chart.component.html',
  styleUrls: ['./icon-chart.component.scss']
})
export class IconChartComponent implements OnInit {

  public myIcon: IconDefinition = faChartPie;
  constructor() { }

  ngOnInit(): void {
  }

}
