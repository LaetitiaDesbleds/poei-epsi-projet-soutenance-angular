import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IconChartComponent } from './icon-chart.component';

describe('IconChartComponent', () => {
  let component: IconChartComponent;
  let fixture: ComponentFixture<IconChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IconChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IconChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
