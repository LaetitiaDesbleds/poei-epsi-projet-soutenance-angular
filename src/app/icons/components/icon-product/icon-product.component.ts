import { Component, OnInit } from '@angular/core';
import {faCaravan, faUser, IconDefinition} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-icon-product',
  templateUrl: './icon-product.component.html',
  styleUrls: ['./icon-product.component.scss']
})
export class IconProductComponent implements OnInit {
  public myIcon: IconDefinition = faCaravan;
  constructor() { }

  ngOnInit(): void {
  }

}
