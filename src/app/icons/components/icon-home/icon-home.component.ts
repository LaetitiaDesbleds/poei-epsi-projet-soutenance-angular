import { Component, OnInit } from '@angular/core';
import {faHome, faTimes, IconDefinition} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-icon-home',
  templateUrl: './icon-home.component.html',
  styleUrls: ['./icon-home.component.scss']
})
export class IconHomeComponent implements OnInit {

  public myIcon: IconDefinition = faHome;

  constructor() { }

  ngOnInit(): void {
  }

}
