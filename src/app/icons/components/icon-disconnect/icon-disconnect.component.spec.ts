import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IconDisconnectComponent } from './icon-disconnect.component';

describe('IconDisconnectComponent', () => {
  let component: IconDisconnectComponent;
  let fixture: ComponentFixture<IconDisconnectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IconDisconnectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IconDisconnectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
