import { Component, OnInit } from '@angular/core';
import {faClosedCaptioning, IconDefinition} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-icon-disconnect',
  templateUrl: './icon-disconnect.component.html',
  styleUrls: ['./icon-disconnect.component.scss']
})
export class IconDisconnectComponent implements OnInit {

  public myIcon: IconDefinition = faClosedCaptioning;
  constructor() { }

  ngOnInit(): void {
  }

}
