import { Component, OnInit } from '@angular/core';
import {faAdd, IconDefinition} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-icon-add',
  templateUrl: './icon-add.component.html',
  styleUrls: ['./icon-add.component.scss']
})
export class IconAddComponent implements OnInit {
  public myIcon: IconDefinition = faAdd;
  constructor() { }

  ngOnInit(): void {
  }

}
