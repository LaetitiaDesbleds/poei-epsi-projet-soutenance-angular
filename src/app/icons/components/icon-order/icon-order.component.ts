import { Component, OnInit } from '@angular/core';
import {faEdit, faListUl, IconDefinition} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-icon-order',
  templateUrl: './icon-order.component.html',
  styleUrls: ['./icon-order.component.scss']
})
export class IconOrderComponent implements OnInit {

  public myIcon: IconDefinition = faListUl;
  constructor() { }

  ngOnInit(): void {
  }

}
