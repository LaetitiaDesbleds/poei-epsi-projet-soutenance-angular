import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {LoginRoutingModule} from './login-routing.module';
import {PageForgotPasswordComponent} from './pages/page-forgot-password/page-forgot-password.component';
import {PageResetPasswordComponent} from './pages/page-reset-password/page-reset-password.component';
import {PageSignInComponent} from './pages/page-sign-in/page-sign-in.component';
import {ReactiveFormsModule} from "@angular/forms";
import {LoginSigninFormComponent} from './components/login-signin-form/login-signin-form.component';
import {LoginSignupFormComponent} from './components/login-signup-form/login-signup-form.component';

@NgModule({
  declarations: [
    PageForgotPasswordComponent,
    PageResetPasswordComponent,
    PageSignInComponent,
    LoginSigninFormComponent,
    LoginSignupFormComponent,
  ],
  imports: [
    CommonModule,
    LoginRoutingModule,
    ReactiveFormsModule
  ],
  exports: [
    LoginSigninFormComponent,
    LoginSignupFormComponent
  ]
})
export class LoginModule {
}
