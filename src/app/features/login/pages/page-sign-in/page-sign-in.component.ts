import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UsersService} from "../../../user-connected/users/users.service";
import {User} from "../../../../core/models/user";
import {Router} from "@angular/router";
import {AuthentificationService} from "../../../../core/services/authentification.service";

@Component({
  selector: 'app-page-sign-in',
  templateUrl: './page-sign-in.component.html',
  styleUrls: ['./page-sign-in.component.scss']
})
export class PageSignInComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
}
