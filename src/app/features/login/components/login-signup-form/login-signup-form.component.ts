import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {User} from "../../../../core/models/user";
import {UsersService} from "../../../user-connected/users/users.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login-signup-form',
  templateUrl: './login-signup-form.component.html',
  styleUrls: ['./login-signup-form.component.scss']
})
export class LoginSignupFormComponent implements OnInit {

  @Input()
  public initSignupUser?: User;

  @Output()
  public submittedUserSignup = new EventEmitter<User>();

  public signupUserForm!: FormGroup;

  constructor(private fb: FormBuilder, private userService: UsersService, private router: Router) { }

  ngOnInit(): void {
    this.signupUserForm = this.fb.group( {
      id: [this.initSignupUser?.id],
      employeeNumber: [this.initSignupUser?.employeeNumber, Validators.required],
      username: [this.initSignupUser?.username, Validators.required],
      lastname: [this.initSignupUser?.lastname],
      firstname: [this.initSignupUser?.firstname],
      email: [this.initSignupUser?.email],
      password: [this.initSignupUser?.password, Validators.required],
      grants: [this.initSignupUser?.grants],
      job: [this.initSignupUser?.job],
    })
  }

  hasError(fieldName: string, error: string) {
    const formControl = this.signupUserForm?.controls[fieldName];
    return formControl?.dirty && formControl?.hasError(error);
  }

  public onSubmitSignup(): void {
    if(this.signupUserForm.invalid) {
      return;
    }
    console.log('test');
    this.userService.addItem(this.signupUserForm.value).subscribe(
      res => {
        // window.location.reload();
        this.router.onSameUrlNavigation = 'reload';
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        this.router.navigateByUrl('/');

      }
    );
  }

}
