import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {User} from "../../../../core/models/user";
import {AuthentificationService} from "../../../../core/services/authentification.service";
import {Router} from "@angular/router";


@Component({
  selector: 'app-login-signin-form',
  templateUrl: './login-signin-form.component.html',
  styleUrls: ['./login-signin-form.component.scss']
})
export class LoginSigninFormComponent implements OnInit {
  public signinUserForm!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthentificationService
  ) {
  }

  ngOnInit(): void {
    this.signinUserForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    })
  }

  hasError(fieldName: string, error: string) {
    const formControl = this.signinUserForm?.controls[fieldName];
    return formControl?.dirty && formControl?.hasError(error);
  }

  public onSubmitSignin(): void {
    if (this.signinUserForm.invalid) {
      return;
    }

    const formValue = this.signinUserForm.value;
    this.authService.login(formValue.username, formValue.password).subscribe(value => {
      this.router.navigate(['/', 'app']);
    })
  }

}
