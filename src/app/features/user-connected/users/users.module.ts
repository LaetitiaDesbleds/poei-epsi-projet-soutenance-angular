import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { PageAddUserComponent } from './pages/page-add-user/page-add-user.component';
import { PageEditUserComponent } from './pages/page-edit-user/page-edit-user.component';
import { PageUsersListComponent } from './pages/page-users-list/page-users-list.component';
import { UserFormComponent } from './components/user-form/user-form.component';
import {ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "../../../shared/shared.module";
import {TemplatesModule} from "../../../templates/templates.module";


@NgModule({
  declarations: [
    PageAddUserComponent,
    PageEditUserComponent,
    PageUsersListComponent,
    UserFormComponent
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    ReactiveFormsModule,
    SharedModule,
    TemplatesModule
  ],
  exports: [
    UserFormComponent
  ]
})
export class UsersModule { }
