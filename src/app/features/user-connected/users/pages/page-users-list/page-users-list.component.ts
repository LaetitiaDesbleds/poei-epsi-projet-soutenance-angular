import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {User} from "../../../../../core/models/user";
import {UsersService} from "../../users.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {
  ConfirmationModalComponent
} from "../../../../../shared/components/confirmation-modal/confirmation-modal.component";

@Component({
  selector: 'app-page-users-list',
  templateUrl: './page-users-list.component.html',
  styleUrls: ['./page-users-list.component.scss']
})
export class PageUsersListComponent implements OnInit {

  public userHeaders = [
    'ID',
    'Nom d\'utilisateur',
    'N°Employé',
    'Nom',
    'Prénom',
    'Email',
    'Rôle',
    'Métier',
    ''
  ];

  public users$!: Observable<User[]>;

  constructor(private userService: UsersService,
              private modalService: NgbModal) { }

  ngOnInit(): void {
    this.users$ = this.userService.getCollection();
  }

  handleDelete(id: number) {
    this.modalService.open(ConfirmationModalComponent).result.then((result) => {
      this.userService.deleteItemById(id).subscribe((response) => {
        this.users$ = this.userService.getCollection();

        alert('Employé supprimer avec succès !');
      });
    })
  }
}
