import { Component, OnInit } from '@angular/core';
import {UserI} from "../../../../../core/interfaces/user-i";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {UsersService} from "../../users.service";

@Component({
  selector: 'app-page-edit-user',
  templateUrl: './page-edit-user.component.html',
  styleUrls: ['./page-edit-user.component.scss']
})
export class PageEditUserComponent implements OnInit {

  public userId = 0;
  public user?: UserI;
  public userFormGroup?: FormGroup;

  constructor(private activatedRoute: ActivatedRoute,
              private usersService: UsersService,
              private fb: FormBuilder,
              private router: Router) { }

  ngOnInit(): void {
    this.activatedRoute.url.subscribe(
      url => {
        const id = url[url.length -1].path;
        this.userId = Number(id);

        this.getUser(this.userId);
      }
    );
  }

  private getUser(id: number) {
    this.usersService.getItemById(id).subscribe(
      (user) => {
        this.user = user;
        this.initForm(user);
      }
    );
  }

  private initForm(user: UserI) {
    this.userFormGroup = this.fb.group({
      employeeNumber: [user.employeeNumber, Validators.required],
      username: [user.username, Validators.required],
      lastname: [user.lastname, Validators.required],
      firstname: [user.firstname, Validators.required],
      email: [user.email, [Validators.required, Validators.email]],
      password: [user.password, Validators.required],
      grants: [user.grants, Validators.required],
      job: [user.job, Validators.required],
    })
  }

  handleSubmit() {
    if (this.userFormGroup?.invalid) {
      return;
    }

    this.usersService.updateItem(this.userId, this.userFormGroup!.value).subscribe((response) => {
      alert('Employé mis à jour avec succès !');
      this.router.navigate(['/app', 'users', 'list']);
    });
  }
}
