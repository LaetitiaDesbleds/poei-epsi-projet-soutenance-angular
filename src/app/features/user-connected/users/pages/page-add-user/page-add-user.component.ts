import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UsersService} from "../../users.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-page-add-user',
  templateUrl: './page-add-user.component.html',
  styleUrls: ['./page-add-user.component.scss']
})
export class PageAddUserComponent implements OnInit {

  userFormGroup?: FormGroup;

  constructor(private fb: FormBuilder,
              private userService: UsersService,
              private router: Router) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.userFormGroup = this.fb.group({
      employeeNumber: ['', Validators.required],
      username: ['', Validators.required],
      lastname: ['', Validators.required],
      firstname: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      grants: ['', Validators.required],
      job: ['', Validators.required],
    });
  }

  handleSubmit() {
    if (this.userFormGroup?.invalid) {
      return;
    }

    console.log('handleSubmitPageAdd');

    this.userService.addItem(this.userFormGroup!.value).subscribe((response) => {
      alert('Employé ajouté avec succès !');
      this.router.navigate(['/app', 'users', 'list']);
    })
  }
}
