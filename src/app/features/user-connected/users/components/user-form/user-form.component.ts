import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {RoleUser} from "../../../../../core/enums/role-user";
import {JobUser} from "../../../../../core/enums/job-user";

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent {

  @Input() form?: FormGroup;

  @Output() submit = new EventEmitter<any>();

  public grantsOption = Object.values(RoleUser);
  public jobOption = Object.values(JobUser);

  constructor(private fb: FormBuilder) { }

  hasError(fieldName: string, error: string) {
    const formControl = this.form?.controls[fieldName];
    return formControl?.dirty && formControl?.hasError(error);
  }

  handleResetForm() {
    this.form?.reset();
  }

  handleSubmit() {
    if (this.form?.invalid) {
      return;
    }

    console.log('formSubmit');
    this.submit.emit();
  }

}
