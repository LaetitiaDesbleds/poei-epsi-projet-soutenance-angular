import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PageUsersListComponent} from "./pages/page-users-list/page-users-list.component";
import {PageAddUserComponent} from "./pages/page-add-user/page-add-user.component";
import {PageEditUserComponent} from "./pages/page-edit-user/page-edit-user.component";

const routes: Routes = [
  {path: '', redirectTo: 'list', pathMatch: 'full'},
  {path: 'list', component: PageUsersListComponent},
  {path: 'add', component: PageAddUserComponent},
  {path: 'edit/:id', component: PageEditUserComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
