import { Injectable } from '@angular/core';
import {environment} from "../../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from "../../../core/models/user";

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private url: string = `${environment.apiUrl}/users`;

  constructor(private httpClient: HttpClient) { }

  getItemById(id: number): Observable<User> {
    return this.httpClient.get<User>(this.url + '/' + id, {withCredentials: true});
  }

  getCollection(): Observable<User[]> {
    return this.httpClient.get<User[]>(this.url, {withCredentials: true});
  }

  deleteItemById(id: number): Observable<void> {
    return this.httpClient.delete<void>(this.url + '/' + id, {withCredentials: true});
  }

  updateItem(id: number, user: User): Observable<void> {
    return this.httpClient.put<void>(this.url + '/' + id, user, {withCredentials: true});
  }

  addItem(user: User): Observable<void> {
    console.log('addItem');
    return this.httpClient.post<void>(this.url, user, {withCredentials: true});
  }

  logout() {
    return this.httpClient.get("/api/v1/logout");
  }
}
