import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {Product} from "../../../../../core/models/product";
import {CategoryProduct} from "../../../../../core/enums/category-product";
import {GearboxProduct} from "../../../../../core/enums/gearbox-product";
import {PictureProduct} from "../../../../../core/enums/picture-product";

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss']
})
export class ProductFormComponent{
  public product: Product = new Product();
  @Input() form?: FormGroup;

  @Output() submit = new EventEmitter<any>();

  public categoryOption = Object.values(CategoryProduct);
  public gearBoxOption = Object.values(GearboxProduct);
  public pictureOption = Object.values(PictureProduct);

  constructor(private fb: FormBuilder) { }

  hasError(fieldName: string, error: string) {
    const formControl = this.form?.controls[fieldName];
    return formControl?.dirty && formControl?.hasError(error);
  }

  handleResetForm() {
    this.form?.reset();
  }

  handleSubmit() {
    if (this.form?.invalid) {
      return;
    }

    this.submit.emit();
  }


}
