import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PageProductsListComponent} from "./pages/page-products-list/page-products-list.component";
import {PageAddProductComponent} from "./pages/page-add-product/page-add-product.component";
import {PageEditProductComponent} from "./pages/page-edit-product/page-edit-product.component";

const routes: Routes = [
  {path: '', redirectTo: 'list', pathMatch: 'full'},
  {path: 'list', component: PageProductsListComponent},
  {path: 'add', component: PageAddProductComponent},
  {path: 'edit/:id', component: PageEditProductComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
