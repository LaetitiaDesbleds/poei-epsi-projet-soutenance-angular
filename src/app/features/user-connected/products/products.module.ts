import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsRoutingModule } from './products-routing.module';
import { PageAddProductComponent } from './pages/page-add-product/page-add-product.component';
import { PageProductsListComponent } from './pages/page-products-list/page-products-list.component';
import { PageEditProductComponent } from './pages/page-edit-product/page-edit-product.component';
import {ReactiveFormsModule} from "@angular/forms";
import { ProductFormComponent } from './components/product-form/product-form.component';
import {TemplatesModule} from "../../../templates/templates.module";
import {IconsModule} from "../../../icons/icons.module";


@NgModule({
  declarations: [
    PageAddProductComponent,
    PageProductsListComponent,
    PageEditProductComponent,
    ProductFormComponent
  ],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    ReactiveFormsModule,
    TemplatesModule,
    IconsModule
  ],
    exports: [
      ProductFormComponent
    ]
})
export class ProductsModule { }
