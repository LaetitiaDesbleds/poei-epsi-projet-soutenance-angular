import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageProductsListComponent } from './page-products-list.component';

describe('PageProductsListComponent', () => {
  let component: PageProductsListComponent;
  let fixture: ComponentFixture<PageProductsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageProductsListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageProductsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
