import { Component, OnInit } from '@angular/core';
import {Product} from "../../../../../core/models/product";
import {Observable, Subscription} from "rxjs";
import {AuthentificationService} from "../../../../../core/services/authentification.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {
  ConfirmationModalComponent
} from "../../../../../shared/components/confirmation-modal/confirmation-modal.component";
import {ProductsService} from "../../products.service";
import {CategoryProduct} from "../../../../../core/enums/category-product";
import {GearboxProduct} from "../../../../../core/enums/gearbox-product";

@Component({
  selector: 'app-page-products-list',
  templateUrl: './page-products-list.component.html',
  styleUrls: ['./page-products-list.component.scss']
})
export class PageProductsListComponent implements OnInit {

  public products$!: Observable<Product[]>;

  public categoryOption = Object.values(CategoryProduct);
  public gearBoxOption = Object.values(GearboxProduct);

  private souscription: Subscription | null = null;

  constructor(private productsService: ProductsService,
              private authentificationService: AuthentificationService,
              private modalService: NgbModal) { }

  ngOnInit(): void {
    this.products$ = this.productsService.getCollection();
  }

  handleDelete(id: number) {
    this.modalService.open(ConfirmationModalComponent).result.then((result) => {
      this.productsService.deleteItemById(id).subscribe((response) => {
        this.products$ = this.productsService.getCollection();

        alert('Produit supprimer avec succès !');
      });
    })
  }

  /*public getByCategory(categoryOption: Product): void {
    this.productsService.getItemByCategory(categoryOption).subscribe(
      () => this.products$ = this.productsService.getCollection()
    );
  }*/

}
