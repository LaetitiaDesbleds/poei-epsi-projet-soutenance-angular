import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {ProductsService} from "../../products.service";

@Component({
  selector: 'app-page-add-product',
  templateUrl: './page-add-product.component.html',
  styleUrls: ['./page-add-product.component.scss']
})
export class PageAddProductComponent implements OnInit {
  productFormGroup?: FormGroup;

  constructor(private fb: FormBuilder,
              private productsService: ProductsService,
              private router: Router) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.productFormGroup = this.fb.group( {
      denomination: ['', Validators.required],
      price_ttc: ['', Validators.required],
      gear_box: ['', Validators.required],
      category: ['', Validators.required],
      numberOfBeds: ['', Validators.required],
      picture: ['', Validators.required],
      yearOfManufacture: ['', Validators.required],
    });
  }

  handleSubmit() {
    if (this.productFormGroup?.invalid) {
      return;
    }

    this.productsService.addItem(this.productFormGroup!.value).subscribe(() => {
      alert('Produit ajouté avec succès !');
      this.router.navigate(['/app', 'products', 'list']);
    })


  }
}
