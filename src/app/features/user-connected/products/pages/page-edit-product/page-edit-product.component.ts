import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ProductI} from "../../../../../core/interfaces/product-i";
import {ProductsService} from "../../products.service";

@Component({
  selector: 'app-page-edit-product',
  templateUrl: './page-edit-product.component.html',
  styleUrls: ['./page-edit-product.component.scss']
})
export class PageEditProductComponent implements OnInit {

  public productId = 0;
  public product?: ProductI;
  public productFormGroup?: FormGroup;

  constructor(private activatedRoute: ActivatedRoute,
              private productsService: ProductsService,
              private fb: FormBuilder,
              private router: Router) { }

  ngOnInit(): void {
    this.activatedRoute.url.subscribe(
      url => {
        const id = url[url.length - 1].path;
        this.productId = Number(id);

        this.getProduct(this.productId);
      }
    );
  }

  private getProduct(id: number) {
    this.productsService.getItemById(id).subscribe(
      (product) => {
        this.product = product;
        this.initForm(product);
      }
    );
  }

  private initForm(product: ProductI) {
    this.productFormGroup = this.fb.group( {
      denomination: [product.denomination, Validators.required],
      price_ttc: [product.price_ttc, Validators.required],
      gear_box: [product.gear_box, Validators.required],
      category: [product.category, Validators.required],
      numberOfBeds: [product.numberOfBeds, Validators.required],
      picture: [product.picture, Validators.required],
      yearOfManufacture: [product.yearOfManufacture, Validators.required],
    });
  }


  handleSubmit() {
    if (this.productFormGroup?.invalid) {
      return;
    }

    this.productsService.updateItem(this.productId, this.productFormGroup!.value).subscribe(() => {
      alert('Produit mis à jour avec succès !');
      this.router.navigate(['/app', 'products', 'list']);
    });

  }

}
