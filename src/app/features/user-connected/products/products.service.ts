import { Injectable } from '@angular/core';
import {environment} from "../../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Product} from "../../../core/models/product";

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  private url: string = `${environment.apiUrl}/products`;

  constructor(private httpClient: HttpClient) { }

  getItemById(id: number): Observable<Product> {
    return this.httpClient.get<Product>(this.url + '/' + id, {withCredentials: true});
  }

  getCollection(): Observable<Product[]> {
    return this.httpClient.get<Product[]>(this.url, {withCredentials: true});
  }

  deleteItemById(id: number): Observable<void> {
    return this.httpClient.delete<void>(this.url + '/' + id, {withCredentials: true});
  }

  updateItem(id: number, product: Product): Observable<void> {
    return this.httpClient.put<void>(this.url + '/' + id, product, {withCredentials: true});
  }

  addItem(product: Product): Observable<void> {
    return this.httpClient.post<void>(this.url, product, {withCredentials: true});
  }

  /*getItemByCategory(product: Product): Observable<Product[]> {
    return this.httpClient.get<Product[]>(this.url + product.category);
  }*/
}
