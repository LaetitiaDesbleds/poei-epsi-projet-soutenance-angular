import {Component, OnInit} from '@angular/core';
import {Order} from "../../../../../core/models/order";
import {OrdersService} from "../../orders.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-page-add-order',
  templateUrl: './page-add-order.component.html',
  styleUrls: ['./page-add-order.component.scss']
})
export class PageAddOrderComponent implements OnInit {
  newOrder = new Order();

  constructor(private orderService: OrdersService, private router: Router) {
  }

  ngOnInit(): void {
  }


  addOrder(order: Order): void {
    this.orderService.addItem(order).subscribe(res => {
        alert("Commande ajoutée avec succès !");
        this.router.navigate(['/app/orders/list']);
      })
  }

}
