import {Component, OnInit} from '@angular/core';
import {Observable, tap} from "rxjs";
import {OrdersService} from "../../orders.service";
import {ActivatedRoute} from "@angular/router";
import {OrderDisplay} from "../../../../../core/models/orderDisplay";
import {
  ConfirmationModalComponent
} from "../../../../../shared/components/confirmation-modal/confirmation-modal.component";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-page-orders-list',
  templateUrl: './page-orders-list.component.html',
  styleUrls: ['./page-orders-list.component.scss']
})
export class PageOrdersListComponent implements OnInit {
  public orderHeaders = [
    'Date',
    'Status',
    'Méthode de Paiement',
    'Nom du client',
    'Produit',
    'Coût'
  ];

  public orders$!: Observable<OrderDisplay[]>;

  constructor(private activatedRoute: ActivatedRoute, private ordersService: OrdersService, private modalService: NgbModal) {
  }


  ngOnInit(): void {
    this.orders$ = this.ordersService.getCollection()
      .pipe(
        tap(value => console.log(value))
      );
  }

  ngOnDestroy() {
  }


  handleDelete(id: number) {
    this.modalService.open(ConfirmationModalComponent).result.then((result) => {
      this.ordersService.deleteItemById(id).subscribe((response) => {
        this.orders$ = this.ordersService.getCollection();

        alert('Commande supprimée avec succès !');
      });
    })

  }
}
