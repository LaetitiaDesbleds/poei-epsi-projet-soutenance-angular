import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {OrdersRoutingModule} from './orders-routing.module';
import {PageAddOrderComponent} from './pages/page-add-order/page-add-order.component';
import {PageEditOrderComponent} from './pages/page-edit-order/page-edit-order.component';
import {PageOrdersListComponent} from './pages/page-orders-list/page-orders-list.component';
import {ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "../../../shared/shared.module";
import {TemplatesModule} from "../../../templates/templates.module";
import {OrderFormComponent} from "./components/order-form/order-form.component";

@NgModule({
  declarations: [
    PageAddOrderComponent,
    PageEditOrderComponent,
    PageOrdersListComponent,
    OrderFormComponent
  ],
  imports: [
    CommonModule,
    OrdersRoutingModule,
    ReactiveFormsModule,
    SharedModule,
    TemplatesModule
  ]
})
export class OrdersModule {
}
