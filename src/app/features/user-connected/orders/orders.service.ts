import {Injectable} from '@angular/core';
import {environment} from "../../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Order} from "../../../core/models/order";
import {OrderDisplay} from "../../../core/models/orderDisplay";

@Injectable({
  providedIn: 'root'
})
export class OrdersService {
  private url: string = environment.apiUrl + '/orders';

  constructor(private httpClient: HttpClient) {
  }

  getItemById(id: number): Observable<Order> {
    return this.httpClient.get<Order>(this.url + '/' + id, {withCredentials: true});
  }

  getCollection(): Observable<OrderDisplay[]> {
    return this.httpClient.get<OrderDisplay[]>(this.url, {withCredentials: true});
  }

  addItem(order: Order): Observable<void> {
    return this.httpClient.post<void>(this.url, order, {withCredentials: true});
  }

  deleteItemById(id: number): Observable<void> {
    return this.httpClient.delete<void>(this.url + '/' + id, {withCredentials: true});
  }

}
