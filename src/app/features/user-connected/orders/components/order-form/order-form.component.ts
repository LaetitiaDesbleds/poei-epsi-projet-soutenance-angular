import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Observable} from "rxjs";
import {Customer} from "../../../../../core/models/customer";
import {Product} from "../../../../../core/models/product";
import {Order} from "../../../../../core/models/order";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CustomersService} from "../../../clients/customers.service";
import {ProductsService} from "../../../products/products.service";
import {OrderStateEnum} from "../../../../../core/enums/orderStateEnum";
import {PaymentMethodEnum} from "../../../../../core/enums/paymentMethodEnum";

@Component({
  selector: 'app-order-form',
  templateUrl: './order-form.component.html',
  styleUrls: ['./order-form.component.scss']
})
export class OrderFormComponent implements OnInit {

  @Input()
  initOrder = new Order();
  public orderStates = Object.values(OrderStateEnum)
  public paymentMethods = Object.values(PaymentMethodEnum)
  public clients$!: Observable<Customer[]>;
  public products$!: Observable<Product[]>;
  public form!: FormGroup;
  @Output()
  public submittedOrder = new EventEmitter<Order>();

  constructor(private fb: FormBuilder, private clientsService: CustomersService, private productsService: ProductsService) {
  }

  ngOnInit(): void {
    this.initForm();
    this.clients$ = this.clientsService.getCollection();
      // .pipe(
        // tap(value => console.log(value)
        // ));
    this.products$ = this.productsService.getCollection();

  }

  initForm() {
    this.form = this.fb.group({
      id: [this.initOrder.id],
      orderDate: [this.initOrder.orderDate, Validators.required],
      orderState: [this.initOrder.orderState],
      paymentMethod: [this.initOrder.paymentMethod, Validators.required],
      customerId: [this.initOrder.customerId, Validators.required],
      productId: [this.initOrder.productId, Validators.required],
      notes: [this.initOrder.orderNotes],
    });
  }

  handleSubmit() {
    // console.log(this.form.value);
    // console.log(this.form.getRawValue());
    if (this.form?.invalid) {
      this.form.markAllAsTouched();
    } else {
      this.submittedOrder.emit(this.form.value);
    }

  }

  hasError(fieldName: string, error: string) {
    const formControl = this.form?.controls[fieldName];
    return formControl?.dirty && formControl?.hasError(error);
  }

  handleResetForm() {
    this.form?.reset();
  }

}
