import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserConnectedRoutingModule } from './user-connected-routing.module';
import { UserConnectedComponent } from './user-connected.component';
import {CoreModule} from "../../core/core.module";


@NgModule({
  declarations: [
    UserConnectedComponent
  ],
  imports: [
    CommonModule,
    UserConnectedRoutingModule,
    CoreModule
  ]
})
export class UserConnectedModule { }
