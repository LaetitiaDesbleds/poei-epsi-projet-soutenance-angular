import { Injectable } from '@angular/core';
import {Customer} from "../../../core/models/customer";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  private url: string = environment.apiUrl + '/customers';
  constructor(private httpClient: HttpClient) { }

  getItemById(id: number): Observable<Customer> {
    return this.httpClient.get<Customer>(this.url + '/' + id, {withCredentials: true});
  }
  getCollection(): Observable<Customer[]> {
    return this.httpClient.get<Customer[]>(this.url, {withCredentials: true});
  }
  deleteItemById(id: number): Observable<void> {
    return this.httpClient.delete<void>(this.url + '/' + id, {withCredentials: true});
  }
  updateItem(id: number, client: Customer): Observable<Customer> {
    return this.httpClient.put<Customer>(this.url + '/' + id, client, {withCredentials: true});
  }
  addItem(client: Customer): Observable<void> {
    return this.httpClient.post<void>(this.url, client, {withCredentials: true});
  }

}
