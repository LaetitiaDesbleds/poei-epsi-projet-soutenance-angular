import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PageCustomersListComponent} from "./pages/page-customer-list/page-customers-list.component";
import {PageAddCustomerComponent} from "./pages/page-add-customer/page-add-customer.component";
import {PageEditCustomerComponent} from "./pages/page-edit-customer/page-edit-customer.component";

const routes: Routes = [
  {path: '', redirectTo: 'list', pathMatch: 'full'},
  {path: 'list', component: PageCustomersListComponent},
  {path: 'add', component: PageAddCustomerComponent},
  {path: 'edit/:id', component: PageEditCustomerComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomersRoutingModule { }
