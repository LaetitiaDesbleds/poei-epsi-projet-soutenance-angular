import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";


@Component({
  selector: 'app-client-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.scss']
})
export class CustomerFormComponent {


  @Input() form?: FormGroup;

  @Output() submit = new EventEmitter<any>();

  constructor(private fb: FormBuilder) { }

  hasError(fieldName: string, error: string) {
    const formControl = this.form?.controls[fieldName];
    return formControl?.dirty && formControl?.hasError(error);
  }

  handleResetForm() {
    this.form?.reset();
  }

  handleSubmit() {
    if (this.form?.invalid) {
      return;
    }

    this.submit.emit();
  }
}
