import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CustomersService} from "../../customers.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-page-add-client',
  templateUrl: './page-add-customer.component.html',
  styleUrls: ['./page-add-customer.component.scss']
})
export class PageAddCustomerComponent implements OnInit {

  clientFormGroup?: FormGroup;

  constructor(private fb: FormBuilder,
              private clientService: CustomersService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.clientFormGroup = this.fb.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      address: [],
      mobile: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
      status: [null, Validators.required],
      notes: [],
    });
  }

  handleSubmit() {
    if (this.clientFormGroup?.invalid) {
      return;
    }

    this.clientService.addItem(this.clientFormGroup!.value).subscribe(() => {
      alert('Client ajouté avec succès !');
      this.router.navigate(['/app', 'clients', 'list']);
    })

  }
}
