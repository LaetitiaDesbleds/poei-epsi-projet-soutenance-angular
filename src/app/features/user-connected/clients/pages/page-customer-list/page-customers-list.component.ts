import {Component, OnInit} from '@angular/core';
import {CustomersService} from "../../customers.service";
import {Customer} from "../../../../../core/models/customer";
import {AuthentificationService} from "../../../../../core/services/authentification.service";
import {Observable} from "rxjs";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {
  ConfirmationModalComponent
} from "../../../../../shared/components/confirmation-modal/confirmation-modal.component";

@Component({
  selector: 'app-page-clients-list',
  templateUrl: './page-customers-list.component.html',
  styleUrls: ['./page-customers-list.component.scss']
})
export class PageCustomersListComponent implements OnInit {

  public clientHeaders = [
    'Nom',
    'Prénom',
    'Email',
    'Adresse',
    'Téléphone',
    'Statut',
  ];

  public clients$!: Observable<Customer[]>;

  constructor(private clientService: CustomersService,
              private authentificationService: AuthentificationService,
              private modalService: NgbModal) {
  }

  ngOnInit(): void {
    this.clients$ = this.clientService.getCollection();
  }

  handleDelete(id: number) {
    this.modalService.open(ConfirmationModalComponent).result.then(() => {
      // Supprimer l'element de la BD
      this.clientService.deleteItemById(id).subscribe(() => {
        this.clients$ = this.clientService.getCollection();
        alert('Client supprimer avec succès !');
      });
    })
  }
}

