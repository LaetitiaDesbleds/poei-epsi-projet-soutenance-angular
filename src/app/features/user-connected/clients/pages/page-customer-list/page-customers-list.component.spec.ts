import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageCustomersListComponent } from './page-customers-list.component';

describe('PageClientsListComponent', () => {
  let component: PageCustomersListComponent;
  let fixture: ComponentFixture<PageCustomersListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageCustomersListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageCustomersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
