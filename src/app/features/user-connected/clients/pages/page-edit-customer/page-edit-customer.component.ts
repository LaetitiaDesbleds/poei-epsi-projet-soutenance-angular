import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {CustomersService} from "../../customers.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CustomerI} from "../../../../../core/interfaces/customer-i";

@Component({
  selector: 'app-page-edit-client',
  templateUrl: './page-edit-customer.component.html',
  styleUrls: ['./page-edit-customer.component.scss']
})
export class PageEditCustomerComponent implements OnInit {

  public clientId = 0;
  public client?: CustomerI;
  public clientFormGroup?: FormGroup;

  constructor(private  activatedRoute: ActivatedRoute,
              private clientService: CustomersService,
              private fb: FormBuilder,
              private router: Router) { }

  ngOnInit(): void {
    this.activatedRoute.url.subscribe(
      url => {
        const id = url[url.length - 1].path;
        this.clientId = Number(id);

        this.getClient(this.clientId);
      }
    );
  }

  private getClient(id: number) {
    this.clientService.getItemById(id).subscribe(
      (client) => {
        this.client = client;
        this.initForm(client);
      }
    );
  }

  private initForm(client: CustomerI) {
    this.clientFormGroup = this.fb.group({
      firstname: [client.firstname, Validators.required],
      lastname: [client.lastname, Validators.required],
      email: [client.email, [Validators.required, Validators.email]],
      address: [client.address],
      mobile: [client.mobile, [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
      status: [client.status, Validators.required],
      notes: [client.notes],
    });
  }

  handleSubmit() {
    if (this.clientFormGroup?.invalid) {
      return;
    }

    this.clientService.updateItem(this.clientId, this.clientFormGroup!.value).subscribe((response) => {
      alert('Client mis à jour avec succès !');
      this.router.navigate(['/app', 'clients', 'list']);

    });
  }
}

