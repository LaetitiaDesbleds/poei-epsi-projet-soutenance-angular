import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomersRoutingModule } from './customers-routing.module';
import { PageAddCustomerComponent } from './pages/page-add-customer/page-add-customer.component';
import { PageEditCustomerComponent } from './pages/page-edit-customer/page-edit-customer.component';
import { PageCustomersListComponent } from './pages/page-customer-list/page-customers-list.component';
import { ReactiveFormsModule } from '@angular/forms';
import {SharedModule} from "../../../shared/shared.module";
import {TemplatesModule} from "../../../templates/templates.module";
import { CustomerFormComponent } from './components/customer-form/customer-form.component';
import {IconsModule} from "../../../icons/icons.module";


@NgModule({
  declarations: [
    PageAddCustomerComponent,
    PageEditCustomerComponent,
    PageCustomersListComponent,
    CustomerFormComponent
  ],
    imports: [
        CommonModule,
        CustomersRoutingModule,
        ReactiveFormsModule,
        SharedModule,
        TemplatesModule,
        IconsModule
    ]
})
export class CustomersModule { }
