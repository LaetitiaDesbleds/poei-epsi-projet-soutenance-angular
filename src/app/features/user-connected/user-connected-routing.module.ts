import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserConnectedComponent} from "./user-connected.component";
import {AuthGuard} from "../../core/guards/auth.guard";
import {RoleUser} from "../../core/enums/role-user";
import {AdminGuard} from "../../core/guards/admin.guard";

const routes: Routes = [
  {
    path: '',
    component: UserConnectedComponent,
    children: [
      {
        canActivate: [AuthGuard],
        path: 'orders', loadChildren:
          () => import('./orders/orders.module')
            .then((module_) => module_.OrdersModule)
      },

      {
        canActivate: [AuthGuard],
        path: 'clients', loadChildren:
          () => import('./clients/customers.module')
            .then((module_) => module_.CustomersModule)
      },

      {
        canActivate: [AuthGuard],
        path: 'products', loadChildren:
          () => import('./products/products.module')
            .then((module_) => module_.ProductsModule)
      },

      {
        canActivate: [AuthGuard, AdminGuard],
        path: 'users', loadChildren:
          () => import('./users/users.module')
            .then((module_) => module_.UsersModule)
      },
      {path: '', pathMatch: 'full', redirectTo: 'orders'},
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserConnectedRoutingModule {
}
